#!/usr/bin/env bash

main() {
  if [[ "$#" != "1" ]]; then
    echo -e "\n[FAIL] Must pass a file type as first arg; exiting." 1>&2
    exit 1
  fi

  case "$1" in
  [Dd] | [Dd]ir | [Dd]irectory | [Dd]irectories)
    type="d"
    ;;
  [Ff] | [Ff]ile | [Ff]iles)
    type="f"
    ;;
  *)
    echo -e "\n[FAIL] Must pass a valid file type (i.e. file or directory); exiting." 1>&2
    exit 1
    ;;
  esac

  insecure_dir_count="$(find / -xdev -type $type -perm -0002 -gid +999 -print | wc -l)" &&
    if [[ "$insecure_dir_count" -ge "1" ]]; then
      echo -e "\n[INFO] Stripping the write bit from 'others' on the following:" &&
        find / -xdev -type "$type" -perm -0002 -gid +999 -print -exec chmod o-w '{}' \; &&
        insecure_dir_count="$(find / -xdev -type $type -perm -0002 -gid +999 -print | wc -l)" &&
        if [[ "$insecure_dir_count" == "0" ]]; then
          echo -e "\n[INFO] Successfully performed all permission adjustments."
          exit 86
        else
          echo -e "\n[FAIL] Unsuccessfully performed one or more permission adjustments; exiting." 1>&2
          echo -e "         $(find / -xdev -type $type -perm -0002 -gid +999 -print)" 1>&2
          exit 1
        fi
    else
      exit 0
    fi
}

main "${1//[^[:alpha:]]/}" ||
  {
    echo -e "\n[FAIL] Script failed with an unhandled error; exiting." 1>&2
    exit 1
  }
