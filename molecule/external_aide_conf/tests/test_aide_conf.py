import hashlib
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_aide_configuration_applied(host):
    test_aide_file = f"{os.environ['MOLECULE_SCENARIO_DIRECTORY']}/aide.conf"

    provided_aide_conf = hashlib.md5(
            open(test_aide_file, "rb").read()
        ).hexdigest().rstrip()
    deployed_aide_conf = host.run(
            "md5sum /etc/aide.conf | cut -d ' ' -f1"
        ).stdout.rstrip()

    assert provided_aide_conf == deployed_aide_conf
