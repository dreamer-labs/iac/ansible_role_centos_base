import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_oscap_installed(host):

    package = host.package("openscap-scanner")
    assert package.is_installed


def test_fips_mode_in_grub(host):

    assert "fips=1" not in host.file("/etc/default/grub").content_string


def test_fips_mode_enabled(host):

    cmd = host.run("cat /proc/sys/crypto/fips_enabled")

    assert "1" not in cmd.stdout
